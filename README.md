
<!-- README.md is generated from README.Rmd. Please edit that file -->

# `{shinyakativ}` is a personal jukebox in shiny

> Version: 2022.02.12.0235 now superseded by 2022.02.12.1338

Not using shiny yet but currently deployed via
<https://j0h0w.gitlab.io/shinyakativ/>.

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
[![pipeline
status](https://gitlab.com/shinyakativ/badges/main/pipeline.svg)](https://gitlab.com/shinyakativ/-/commits/main)
<!-- badges: end -->

``` r
lapply(list.files(system.file("extdata", "mp3", package="shinyakativ"), full.names = TRUE), 
av::av_media_info)
#> [[1]]
#> [[1]]$duration
#> [1] 169.8547
#> 
#> [[1]]$video
#> NULL
#> 
#> [[1]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[2]]
#> [[2]]$duration
#> [1] 51.77469
#> 
#> [[2]]$video
#> NULL
#> 
#> [[2]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[3]]
#> [[3]]$duration
#> [1] 114.0245
#> 
#> [[3]]$video
#> NULL
#> 
#> [[3]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[4]]
#> [[4]]$duration
#> [1] 112.1062
#> 
#> [[4]]$video
#> NULL
#> 
#> [[4]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[5]]
#> [[5]]$duration
#> [1] 177.2669
#> 
#> [[5]]$video
#> NULL
#> 
#> [[5]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[6]]
#> [[6]]$duration
#> [1] 187.473
#> 
#> [[6]]$video
#> NULL
#> 
#> [[6]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[7]]
#> [[7]]$duration
#> [1] 147.0171
#> 
#> [[7]]$video
#> NULL
#> 
#> [[7]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[8]]
#> [[8]]$duration
#> [1] 147.0171
#> 
#> [[8]]$video
#> NULL
#> 
#> [[8]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[9]]
#> [[9]]$duration
#> [1] 112.4681
#> 
#> [[9]]$video
#> NULL
#> 
#> [[9]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[10]]
#> [[10]]$duration
#> [1] 52.271
#> 
#> [[10]]$video
#> NULL
#> 
#> [[10]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[11]]
#> [[11]]$duration
#> [1] 80.79669
#> 
#> [[11]]$video
#> NULL
#> 
#> [[11]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[12]]
#> [[12]]$duration
#> [1] 129.4232
#> 
#> [[12]]$video
#> NULL
#> 
#> [[12]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[13]]
#> [[13]]$duration
#> [1] 64.34831
#> 
#> [[13]]$video
#> NULL
#> 
#> [[13]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        1       44100 mp3float     NA  128000   mono
#> 
#> 
#> [[14]]
#> [[14]]$duration
#> [1] 60.68838
#> 
#> [[14]]$video
#> NULL
#> 
#> [[14]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[15]]
#> [[15]]$duration
#> [1] 69.27675
#> 
#> [[15]]$video
#> NULL
#> 
#> [[15]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[16]]
#> [[16]]$duration
#> [1] 146.7058
#> 
#> [[16]]$video
#> NULL
#> 
#> [[16]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[17]]
#> [[17]]$duration
#> [1] 93.3185
#> 
#> [[17]]$video
#> NULL
#> 
#> [[17]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[18]]
#> [[18]]$duration
#> [1] 120.529
#> 
#> [[18]]$video
#> NULL
#> 
#> [[18]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[19]]
#> [[19]]$duration
#> [1] 133.5118
#> 
#> [[19]]$video
#> NULL
#> 
#> [[19]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[20]]
#> [[20]]$duration
#> [1] 133.5118
#> 
#> [[20]]$video
#> NULL
#> 
#> [[20]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[21]]
#> [[21]]$duration
#> [1] 94.12838
#> 
#> [[21]]$video
#> NULL
#> 
#> [[21]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[22]]
#> [[22]]$duration
#> [1] 93.91019
#> 
#> [[22]]$video
#> NULL
#> 
#> [[22]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[23]]
#> [[23]]$duration
#> [1] 67.37156
#> 
#> [[23]]$video
#> NULL
#> 
#> [[23]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        1       44100 mp3float     NA  128000   mono
#> 
#> 
#> [[24]]
#> [[24]]$duration
#> [1] 167.3045
#> 
#> [[24]]$video
#> NULL
#> 
#> [[24]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[25]]
#> [[25]]$duration
#> [1] 156.2968
#> 
#> [[25]]$video
#> NULL
#> 
#> [[25]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[26]]
#> [[26]]$duration
#> [1] 160.649
#> 
#> [[26]]$video
#> NULL
#> 
#> [[26]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  190140 stereo
#> 
#> 
#> [[27]]
#> [[27]]$duration
#> [1] 156.2968
#> 
#> [[27]]$video
#> NULL
#> 
#> [[27]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[28]]
#> [[28]]$duration
#> [1] 33.41063
#> 
#> [[28]]$video
#> NULL
#> 
#> [[28]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[29]]
#> [[29]]$duration
#> [1] 201.7132
#> 
#> [[29]]$video
#> NULL
#> 
#> [[29]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  190151 stereo
#> 
#> 
#> [[30]]
#> [[30]]$duration
#> [1] 80.20375
#> 
#> [[30]]$video
#> NULL
#> 
#> [[30]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[31]]
#> [[31]]$duration
#> [1] 137.1429
#> 
#> [[31]]$video
#> NULL
#> 
#> [[31]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[32]]
#> [[32]]$duration
#> [1] 96.60081
#> 
#> [[32]]$video
#> NULL
#> 
#> [[32]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[33]]
#> [[33]]$duration
#> [1] 83.01712
#> 
#> [[33]]$video
#> NULL
#> 
#> [[33]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[34]]
#> [[34]]$duration
#> [1] 123.0106
#> 
#> [[34]]$video
#> NULL
#> 
#> [[34]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[35]]
#> [[35]]$duration
#> [1] 49.52819
#> 
#> [[35]]$video
#> NULL
#> 
#> [[35]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[36]]
#> [[36]]$duration
#> [1] 127.2686
#> 
#> [[36]]$video
#> NULL
#> 
#> [[36]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[37]]
#> [[37]]$duration
#> [1] 129.7763
#> 
#> [[37]]$video
#> NULL
#> 
#> [[37]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[38]]
#> [[38]]$duration
#> [1] 115.1083
#> 
#> [[38]]$video
#> NULL
#> 
#> [[38]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       22050 mp3float     NA   80000 stereo
#> 
#> 
#> [[39]]
#> [[39]]$duration
#> [1] 117.7861
#> 
#> [[39]]$video
#> NULL
#> 
#> [[39]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[40]]
#> [[40]]$duration
#> [1] 96.21837
#> 
#> [[40]]$video
#> NULL
#> 
#> [[40]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[41]]
#> [[41]]$duration
#> [1] 53.52488
#> 
#> [[41]]$video
#> NULL
#> 
#> [[41]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[42]]
#> [[42]]$duration
#> [1] 140.0163
#> 
#> [[42]]$video
#> NULL
#> 
#> [[42]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[43]]
#> [[43]]$duration
#> [1] 161.7763
#> 
#> [[43]]$video
#> NULL
#> 
#> [[43]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[44]]
#> [[44]]$duration
#> [1] 127.2686
#> 
#> [[44]]$video
#> NULL
#> 
#> [[44]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[45]]
#> [[45]]$duration
#> [1] 120.0065
#> 
#> [[45]]$video
#> NULL
#> 
#> [[45]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[46]]
#> [[46]]$duration
#> [1] 70.24325
#> 
#> [[46]]$video
#> NULL
#> 
#> [[46]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[47]]
#> [[47]]$duration
#> [1] 176.9013
#> 
#> [[47]]$video
#> NULL
#> 
#> [[47]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[48]]
#> [[48]]$duration
#> [1] 101.783
#> 
#> [[48]]$video
#> NULL
#> 
#> [[48]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[49]]
#> [[49]]$duration
#> [1] 55.22288
#> 
#> [[49]]$video
#> NULL
#> 
#> [[49]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[50]]
#> [[50]]$duration
#> [1] 117.177
#> 
#> [[50]]$video
#> NULL
#> 
#> [[50]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[51]]
#> [[51]]$duration
#> [1] 167.3045
#> 
#> [[51]]$video
#> NULL
#> 
#> [[51]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[52]]
#> [[52]]$duration
#> [1] 131.3959
#> 
#> [[52]]$video
#> NULL
#> 
#> [[52]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[53]]
#> [[53]]$duration
#> [1] 118.1779
#> 
#> [[53]]$video
#> NULL
#> 
#> [[53]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[54]]
#> [[54]]$duration
#> [1] 69.51863
#> 
#> [[54]]$video
#> NULL
#> 
#> [[54]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[55]]
#> [[55]]$duration
#> [1] 153.2865
#> 
#> [[55]]$video
#> NULL
#> 
#> [[55]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[56]]
#> [[56]]$duration
#> [1] 146.7058
#> 
#> [[56]]$video
#> NULL
#> 
#> [[56]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[57]]
#> [[57]]$duration
#> [1] 58.30531
#> 
#> [[57]]$video
#> NULL
#> 
#> [[57]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[58]]
#> [[58]]$duration
#> [1] 96.63494
#> 
#> [[58]]$video
#> NULL
#> 
#> [[58]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[59]]
#> [[59]]$duration
#> [1] 96.62694
#> 
#> [[59]]$video
#> NULL
#> 
#> [[59]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[60]]
#> [[60]]$duration
#> [1] 74.031
#> 
#> [[60]]$video
#> NULL
#> 
#> [[60]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[61]]
#> [[61]]$duration
#> [1] 114.0245
#> 
#> [[61]]$video
#> NULL
#> 
#> [[61]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[62]]
#> [[62]]$duration
#> [1] 134.8963
#> 
#> [[62]]$video
#> NULL
#> 
#> [[62]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[63]]
#> [[63]]$duration
#> [1] 93.47374
#> 
#> [[63]]$video
#> NULL
#> 
#> [[63]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  203295 stereo
#> 
#> 
#> [[64]]
#> [[64]]$duration
#> [1] 97.70817
#> 
#> [[64]]$video
#> NULL
#> 
#> [[64]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[65]]
#> [[65]]$duration
#> [1] 75.43817
#> 
#> [[65]]$video
#> NULL
#> 
#> [[65]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  262279 stereo
#> 
#> 
#> [[66]]
#> [[66]]$duration
#> [1] 112.4681
#> 
#> [[66]]$video
#> NULL
#> 
#> [[66]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[67]]
#> [[67]]$duration
#> [1] 175.541
#> 
#> [[67]]$video
#> NULL
#> 
#> [[67]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  218351 stereo
#> 
#> 
#> [[68]]
#> [[68]]$duration
#> [1] 128.0315
#> 
#> [[68]]$video
#> NULL
#> 
#> [[68]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[69]]
#> [[69]]$duration
#> [1] 72.929
#> 
#> [[69]]$video
#> NULL
#> 
#> [[69]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[70]]
#> [[70]]$duration
#> [1] 138.6372
#> 
#> [[70]]$video
#> NULL
#> 
#> [[70]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[71]]
#> [[71]]$duration
#> [1] 192.8935
#> 
#> [[71]]$video
#> NULL
#> 
#> [[71]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[72]]
#> [[72]]$duration
#> [1] 152.5029
#> 
#> [[72]]$video
#> NULL
#> 
#> [[72]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[73]]
#> [[73]]$duration
#> [1] 98.52994
#> 
#> [[73]]$video
#> NULL
#> 
#> [[73]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  240312 stereo
#> 
#> 
#> [[74]]
#> [[74]]$duration
#> [1] 141.6639
#> 
#> [[74]]$video
#> NULL
#> 
#> [[74]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[75]]
#> [[75]]$duration
#> [1] 88.72106
#> 
#> [[75]]$video
#> NULL
#> 
#> [[75]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  215993 stereo
#> 
#> 
#> [[76]]
#> [[76]]$duration
#> [1] 126.6085
#> 
#> [[76]]$video
#> NULL
#> 
#> [[76]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[77]]
#> [[77]]$duration
#> [1] 194.1871
#> 
#> [[77]]$video
#> NULL
#> 
#> [[77]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[78]]
#> [[78]]$duration
#> [1] 138.7886
#> 
#> [[78]]$video
#> NULL
#> 
#> [[78]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[79]]
#> [[79]]$duration
#> [1] 147.0694
#> 
#> [[79]]$video
#> NULL
#> 
#> [[79]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[80]]
#> [[80]]$duration
#> [1] 138.7886
#> 
#> [[80]]$video
#> NULL
#> 
#> [[80]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[81]]
#> [[81]]$duration
#> [1] 138.7966
#> 
#> [[81]]$video
#> NULL
#> 
#> [[81]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[82]]
#> [[82]]$duration
#> [1] 178.2937
#> 
#> [[82]]$video
#> NULL
#> 
#> [[82]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[83]]
#> [[83]]$duration
#> [1] 147.0774
#> 
#> [[83]]$video
#> NULL
#> 
#> [[83]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[84]]
#> [[84]]$duration
#> [1] 64.163
#> 
#> [[84]]$video
#> NULL
#> 
#> [[84]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[85]]
#> [[85]]$duration
#> [1] 115.1083
#> 
#> [[85]]$video
#> NULL
#> 
#> [[85]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       22050 mp3float     NA   80000 stereo
#> 
#> 
#> [[86]]
#> [[86]]$duration
#> [1] 149.4169
#> 
#> [[86]]$video
#> NULL
#> 
#> [[86]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[87]]
#> [[87]]$duration
#> [1] 154.4621
#> 
#> [[87]]$video
#> NULL
#> 
#> [[87]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[88]]
#> [[88]]$duration
#> [1] 90.34025
#> 
#> [[88]]$video
#> NULL
#> 
#> [[88]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[89]]
#> [[89]]$duration
#> [1] 249.2343
#> 
#> [[89]]$video
#> NULL
#> 
#> [[89]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[90]]
#> [[90]]$duration
#> [1] 132.5017
#> 
#> [[90]]$video
#> NULL
#> 
#> [[90]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        1       44100 mp3float     NA  128000   mono
#> 
#> 
#> [[91]]
#> [[91]]$duration
#> [1] 100.3984
#> 
#> [[91]]$video
#> NULL
#> 
#> [[91]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[92]]
#> [[92]]$duration
#> [1] 149.4089
#> 
#> [[92]]$video
#> NULL
#> 
#> [[92]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[93]]
#> [[93]]$duration
#> [1] 58.70287
#> 
#> [[93]]$video
#> NULL
#> 
#> [[93]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[94]]
#> [[94]]$duration
#> [1] 60.68838
#> 
#> [[94]]$video
#> NULL
#> 
#> [[94]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[95]]
#> [[95]]$duration
#> [1] 208.6661
#> 
#> [[95]]$video
#> NULL
#> 
#> [[95]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[96]]
#> [[96]]$duration
#> [1] 92.70431
#> 
#> [[96]]$video
#> NULL
#> 
#> [[96]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[97]]
#> [[97]]$duration
#> [1] 187.0175
#> 
#> [[97]]$video
#> NULL
#> 
#> [[97]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[98]]
#> [[98]]$duration
#> [1] 129.9472
#> 
#> [[98]]$video
#> NULL
#> 
#> [[98]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[99]]
#> [[99]]$duration
#> [1] 130.1159
#> 
#> [[99]]$video
#> NULL
#> 
#> [[99]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[100]]
#> [[100]]$duration
#> [1] 108.6685
#> 
#> [[100]]$video
#> NULL
#> 
#> [[100]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[101]]
#> [[101]]$duration
#> [1] 258.4265
#> 
#> [[101]]$video
#> NULL
#> 
#> [[101]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  163148 stereo
#> 
#> 
#> [[102]]
#> [[102]]$duration
#> [1] 161.7763
#> 
#> [[102]]$video
#> NULL
#> 
#> [[102]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[103]]
#> [[103]]$duration
#> [1] 93.101
#> 
#> [[103]]$video
#> NULL
#> 
#> [[103]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[104]]
#> [[104]]$duration
#> [1] 177.2669
#> 
#> [[104]]$video
#> NULL
#> 
#> [[104]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[105]]
#> [[105]]$duration
#> [1] 204.6633
#> 
#> [[105]]$video
#> NULL
#> 
#> [[105]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[106]]
#> [[106]]$duration
#> [1] 96.21837
#> 
#> [[106]]$video
#> NULL
#> 
#> [[106]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[107]]
#> [[107]]$duration
#> [1] 127.2686
#> 
#> [[107]]$video
#> NULL
#> 
#> [[107]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[108]]
#> [[108]]$duration
#> [1] 140.0163
#> 
#> [[108]]$video
#> NULL
#> 
#> [[108]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[109]]
#> [[109]]$duration
#> [1] 176.9013
#> 
#> [[109]]$video
#> NULL
#> 
#> [[109]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[110]]
#> [[110]]$duration
#> [1] 101.783
#> 
#> [[110]]$video
#> NULL
#> 
#> [[110]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[111]]
#> [[111]]$duration
#> [1] 48.19594
#> 
#> [[111]]$video
#> NULL
#> 
#> [[111]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[112]]
#> [[112]]$duration
#> [1] 52.4275
#> 
#> [[112]]$video
#> NULL
#> 
#> [[112]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[113]]
#> [[113]]$duration
#> [1] 69.51863
#> 
#> [[113]]$video
#> NULL
#> 
#> [[113]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[114]]
#> [[114]]$duration
#> [1] 169.8547
#> 
#> [[114]]$video
#> NULL
#> 
#> [[114]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  192000 stereo
#> 
#> 
#> [[115]]
#> [[115]]$duration
#> [1] 167.3045
#> 
#> [[115]]$video
#> NULL
#> 
#> [[115]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[116]]
#> [[116]]$duration
#> [1] 168.9077
#> 
#> [[116]]$video
#> NULL
#> 
#> [[116]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[117]]
#> [[117]]$duration
#> [1] 182.0213
#> 
#> [[117]]$video
#> NULL
#> 
#> [[117]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[118]]
#> [[118]]$duration
#> [1] 105.3707
#> 
#> [[118]]$video
#> NULL
#> 
#> [[118]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[119]]
#> [[119]]$duration
#> [1] 179.0233
#> 
#> [[119]]$video
#> NULL
#> 
#> [[119]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[120]]
#> [[120]]$duration
#> [1] 167.1837
#> 
#> [[120]]$video
#> NULL
#> 
#> [[120]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[121]]
#> [[121]]$duration
#> [1] 114.7271
#> 
#> [[121]]$video
#> NULL
#> 
#> [[121]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[122]]
#> [[122]]$duration
#> [1] 149.4089
#> 
#> [[122]]$video
#> NULL
#> 
#> [[122]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[123]]
#> [[123]]$duration
#> [1] 149.4089
#> 
#> [[123]]$video
#> NULL
#> 
#> [[123]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[124]]
#> [[124]]$duration
#> [1] 158.4481
#> 
#> [[124]]$video
#> NULL
#> 
#> [[124]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[125]]
#> [[125]]$duration
#> [1] 170.5012
#> 
#> [[125]]$video
#> NULL
#> 
#> [[125]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[126]]
#> [[126]]$duration
#> [1] 153.8351
#> 
#> [[126]]$video
#> NULL
#> 
#> [[126]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[127]]
#> [[127]]$duration
#> [1] 194.6645
#> 
#> [[127]]$video
#> NULL
#> 
#> [[127]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[128]]
#> [[128]]$duration
#> [1] 208.8553
#> 
#> [[128]]$video
#> NULL
#> 
#> [[128]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  173180 stereo
#> 
#> 
#> [[129]]
#> [[129]]$duration
#> [1] 141.009
#> 
#> [[129]]$video
#> NULL
#> 
#> [[129]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[130]]
#> [[130]]$duration
#> [1] 206.0539
#> 
#> [[130]]$video
#> NULL
#> 
#> [[130]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[131]]
#> [[131]]$duration
#> [1] 172.1718
#> 
#> [[131]]$video
#> NULL
#> 
#> [[131]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[132]]
#> [[132]]$duration
#> [1] 215.6408
#> 
#> [[132]]$video
#> NULL
#> 
#> [[132]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[133]]
#> [[133]]$duration
#> [1] 126.9813
#> 
#> [[133]]$video
#> NULL
#> 
#> [[133]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[134]]
#> [[134]]$duration
#> [1] 150.5176
#> 
#> [[134]]$video
#> NULL
#> 
#> [[134]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[135]]
#> [[135]]$duration
#> [1] 177.6849
#> 
#> [[135]]$video
#> NULL
#> 
#> [[135]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[136]]
#> [[136]]$duration
#> [1] 177.2669
#> 
#> [[136]]$video
#> NULL
#> 
#> [[136]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[137]]
#> [[137]]$duration
#> [1] 153.2865
#> 
#> [[137]]$video
#> NULL
#> 
#> [[137]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[138]]
#> [[138]]$duration
#> [1] 82.94688
#> 
#> [[138]]$video
#> NULL
#> 
#> [[138]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[139]]
#> [[139]]$duration
#> [1] 82.94688
#> 
#> [[139]]$video
#> NULL
#> 
#> [[139]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[140]]
#> [[140]]$duration
#> [1] 74.031
#> 
#> [[140]]$video
#> NULL
#> 
#> [[140]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[141]]
#> [[141]]$duration
#> [1] 105.1361
#> 
#> [[141]]$video
#> NULL
#> 
#> [[141]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        1       44100 mp3float     NA  128000   mono
#> 
#> 
#> [[142]]
#> [[142]]$duration
#> [1] 103.4881
#> 
#> [[142]]$video
#> NULL
#> 
#> [[142]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  160000 stereo
#> 
#> 
#> [[143]]
#> [[143]]$duration
#> [1] 52.59412
#> 
#> [[143]]$video
#> NULL
#> 
#> [[143]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        1       44100 mp3float     NA  128000   mono
#> 
#> 
#> [[144]]
#> [[144]]$duration
#> [1] 51.77469
#> 
#> [[144]]$video
#> NULL
#> 
#> [[144]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[145]]
#> [[145]]$duration
#> [1] 51.77469
#> 
#> [[145]]$video
#> NULL
#> 
#> [[145]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[146]]
#> [[146]]$duration
#> [1] 98.02106
#> 
#> [[146]]$video
#> NULL
#> 
#> [[146]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        1       44100 mp3float     NA  128000   mono
#> 
#> 
#> [[147]]
#> [[147]]$duration
#> [1] 82.94688
#> 
#> [[147]]$video
#> NULL
#> 
#> [[147]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
#> 
#> 
#> [[148]]
#> [[148]]$duration
#> [1] 82.94688
#> 
#> [[148]]$video
#> NULL
#> 
#> [[148]]$audio
#>   channels sample_rate    codec frames bitrate layout
#> 1        2       44100 mp3float     NA  128000 stereo
```
