% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/body_footer.R
\name{body_footer}
\alias{body_footer}
\title{Add content above basic HTML5 footer template}
\usage{
body_footer(x = body_header(jukebox_body()))
}
\arguments{
\item{x}{Content to be added before footer}
}
\value{
A characeter string
}
\description{
Add content above basic HTML5 footer template
}
\examples{
(tmp <- body_footer())
}
