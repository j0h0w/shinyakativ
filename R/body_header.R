#' Add body content below basic HTML5 header template
#'
#' @param x Body content
#' @param outdir Output folder
#' @param css Style header ('<link rel=..')
#' @param title Page title
#'
#' @return A character string
#' @export
#'
#' @examples
#' (tmp <- body_header())
body_header <- function(x = jukebox_body(),
                        outdir = tempdir(),
                        css =
                          header_stylesheets(addFiles =
                                               !file.exists(file.path(outdir,
                                               "DESCRIPTION")), outdir=outdir),
                        title = "Shiny Akativ") {
  outObj <-   glue::glue("<!DOCTYPE html>
    <html>
    <head>
    {css}
    <title>{title}</title>
    </head>
    <body>{x}")
  invisible(outObj)
}

