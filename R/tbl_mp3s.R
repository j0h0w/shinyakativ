#' Metadata for audio files
#'
#' @param src A list of valid audio files (absolute file paths) to be included
#' @param rmCols Characters vector of columns names to remove from output
#'
#' @return A tibble containing metadata
#' @export
#'
#' @importFrom av av_media_info
#' @importFrom dplyr bind_rows mutate
#' @importFrom tidyr as_tibble
#'
#' @examples
#' (test <- tbl_mp3s())
tbl_mp3s <-
  function(src = list.files(system.file("extdata", "mp3", package = "shinyakativ"),
                            full.names = TRUE),
           rmCols = c("frames")) {
    tmp <- lapply(src, av::av_media_info)
    names(tmp) <- basename(src)
    outObj <-
      dplyr::bind_rows(lapply(lapply(tmp, "[[", "audio"), tidyr::as_tibble)) %>%
      dplyr::mutate(filename = basename(src),
                    fullpath = src)
    if (!is.null(rmCols)) {
      outObj <-   outObj %>% dplyr::select(-{
        {
          rmCols
        }
      })
    }
    if (!is.null(rownames(outObj))) {
      rownames(outObj) <- NULL
    }
    invisible(outObj)
  }
